import React, {Component} from 'react';
import {FlatList, Modal, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {modalVisibleHandler, updateItems} from "./src/store/actions";
import Item from "./src/components/Item/Item";
import FullItem from "./src/components/FullItem/FullItem";


class App extends Component {
	componentDidMount() {
		this.props.updateItems();
	}
	
	state = {
		contact: {
			name: '',
			phone: '',
			photo: '',
			email: ''
		}
	};
	
	contactClickHandler = (id) => {
		let contact = [...this.state.contact];
		const index = this.props.contacts.findIndex(p => p.key === id);
		contact = this.props.contacts[index];
		this.setState({contact});
		this.props.modalVisibleHandler(true);
	};
	
	render() {
		return (
			<View style={styles.container}>
				<Modal
					animationType="slide"
					transparent={false}
					visible={this.props.modalVisible || false}
					onRequestClose={() => this.props.modalVisibleHandler(false)}>
					<View style={{marginTop: 22}}>
						<FullItem contact={this.state.contact}/>
					</View>
				</Modal>
				
				<FlatList
					data={this.props.contacts}
					renderItem={(info) => (
						<Item click={() => this.contactClickHandler(info.item.key)}
						      image={{uri: info.item.photo}}
						      title={info.item.name}/>
					)}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'flex-start',
		padding: 10
	}
});

const mapStateToProps = state => {
	return {
		contacts: state.contacts,
		modalVisible: state.modalVisible
	}
};

const mapDispatchToProps = dispatch => {
	return {
		updateItems: () => dispatch(updateItems()),
		modalVisibleHandler: (visible) => dispatch(modalVisibleHandler(visible))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(App);