import axios from "axios/index";

import * as actionsTypes from "../store/actionsTypes";

export const redditTodoRequest = () => {
	return {type: actionsTypes.CONTACTS_TODO_REQUEST};
};

export const redditTodoSuccess = (items) => {
	return {type: actionsTypes.CONTACTS_TODO_SUCCESS, items};
};

export const redditTodoError = (error) => {
	return {type: actionsTypes.CONTACTS_TODO_ERROR, error};
};

export const modalVisibleHandler = (visible) => {
	return {type: actionsTypes.MODAL_VISIBLE, visible};
};


export const updateItems = () => {
	return dispatch => {
		dispatch(redditTodoRequest());
		axios.get('/contacts.json').then(response => {
			if (response.data) dispatch(redditTodoSuccess(response.data));
		}, error => {
			dispatch(redditTodoError(error));
		});
	}
};

