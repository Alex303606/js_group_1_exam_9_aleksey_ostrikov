import * as actionsTypes from "./actionsTypes";

const initialState = {
	contacts: [],
	modalVisible: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionsTypes.CONTACTS_TODO_SUCCESS:
			return {
				...state.contacts,
				contacts: Object.keys(action.items).map(item => {
					return {...action.items[item], key: item};
				}),
			};
		case actionsTypes.MODAL_VISIBLE:
			return {...state, modalVisible: action.visible};
		default:
			return state;
	}
};

export default reducer;