import React from 'react'
import {Image, StyleSheet, Text, View} from "react-native";

const FullItem = props => {
	return (
		<View style={styles.mainContainer}>
			<View style={styles.titleContainer}>
				<Text style={styles.textTitle}>{props.contact.name}</Text>
			</View>
			<View style={styles.rowFlex}>
				<View>
					<Image style={styles.img}  source={{uri: props.contact.photo}}/>
				</View>
				<View style={styles.container}>
					<Text style={styles.text}>{props.contact.phone}</Text>
					<Text style={styles.text}>{props.contact.email}</Text>
				</View>
			</View>
		</View>
	)
};

const styles = StyleSheet.create({
	container: {
		flex: 0,
		flexDirection: 'column',
		backgroundColor: '#fff',
		width: '70%',
		paddingLeft: 20
	},
	text: {
		fontWeight: 'bold',
		fontSize: 16,
		color: '#000',
		marginBottom: 20
	},
	rowFlex: {
		flexDirection: 'row',
	},
	textTitle: {
		fontWeight: 'bold',
		fontSize: 30,
		color: '#000',
		marginBottom: 30
	},
	img: {
		width: 150,
		height: 150,
		marginBottom: 30
	},
	mainContainer: {
		flex: 0,
		flexDirection: 'column',
		backgroundColor: '#fff',
		justifyContent: 'space-between',
		width: '100%',
		padding: 10
	},
	titleContainer: {
		width: '100%'
	}
});

export default FullItem;