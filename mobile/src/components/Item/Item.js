import React from 'react'
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";

const Item = props => {
	return (
		<View>
			<TouchableOpacity style={styles.item} onPress={props.click}>
				<Image source={props.image} style={styles.image}/>
				<Text style={styles.text}>{props.title}</Text>
			</TouchableOpacity>
		</View>
	)
};

const styles = StyleSheet.create({
	item: {
		flexDirection: 'row',
		alignItems: 'center',
		width: '100%',
		backgroundColor: '#eee',
		marginBottom: 20,
		padding: 10
	},
	image: {
		width: '20%',
		height: 100
	},
	text: {
		paddingRight: 10,
		paddingLeft: 10,
		fontSize: 20,
		width: '80%'
	}
});

export default Item;