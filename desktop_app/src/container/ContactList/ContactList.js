import React, {Component, Fragment} from 'react'
import './ContactList.css';
import {connect} from 'react-redux';
import {purchaseCancelHandler, purchaseHandler, updateContact} from "../../store/actions/actionsList";
import Contact from "../../components/Contact/Contact";
import Spinner from "../../components/UI/Spinner/Spinner";
import Modal from "../../components/UI/Modal/Modal";
import FullContact from "../../components/FullContact/FullContact";

class ContactList extends Component {
	
	state = {
		id: null
	};
	
	componentDidMount() {
		this.props.updateContact();
	};
	
	onContactHandler = (id) => {
		this.setState({id});
		this.props.purchaseHandler();
	};
	
	render() {
		const contacts = (
			Object.keys(this.props.contacts).map(contact => {
				return (
					<Contact
						key={contact}
						name={this.props.contacts[contact].name}
						url={this.props.contacts[contact].photo}
						click={() => this.onContactHandler(contact)}
					/>
				)
			})
		);
		
		return (
			<Fragment>
				<Modal
					show={this.props.purchasing}
					closed={this.props.purchaseCancelHandler}
				>
					<FullContact {...this.props} id={this.state.id}/>
				</Modal>
				
				<div className="contact_list">
					{this.props.loading ? <Spinner/> : contacts}
				</div>
			</Fragment>
		)
	}
}

const mapStateToProps = state => {
	return {
		contacts: state.list.contacts,
		loading: state.list.loading,
		purchasing: state.list.purchasing
	}
};

const mapDispatchToProps = dispatch => {
	return {
		updateContact: () => dispatch(updateContact()),
		purchaseCancelHandler: () => dispatch(purchaseCancelHandler()),
		purchaseHandler: () => dispatch(purchaseHandler())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);