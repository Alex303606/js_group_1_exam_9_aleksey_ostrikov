import React, {Component} from 'react'
import {connect} from 'react-redux';
import './AddNewContact.css';
import {NavLink} from "react-router-dom";
import {addContactHandler, editContactHandler} from "../../store/actions/actionsAdd";
import {updateContact} from "../../store/actions/actionsList";
import axios from 'axios';

class AddNewContact extends Component {
	state = {
		contact: {
			name: '',
			phone: '',
			email: '',
			photo: ''
		}
	};
	
	componentDidMount() {
		if (this.props.match.params.id) {
			const id = this.props.match.params.id;
			if(!this.props.contacts[id]) {
				axios.get(`/contacts/${id}.json`).then(response => {
					this.setState({contact: response.data});
				});
			} else {
				this.setState({contact: this.props.contacts[id]});
			}
		}
	}
	
	inputFormHandler = (event) => {
		let contact = {...this.state.contact};
		contact[event.target.name] = event.target.value;
		this.setState({contact});
	};
	
	addContact = (event) => {
		event.preventDefault();
		if (this.props.match.params.id) {
			this.props.editContactHandler(this.props.match.params.id, this.state.contact)
		} else {
			this.props.addContactHandler(this.state.contact);
		}
		const contact = {...this.state.contact};
		Object.keys(this.state.contact).map(key => {
			return contact[key] = '';
		});
		this.setState({contact});
	};
	
	render() {
		return (
			<div className="add_new_contact">
				<div className="add_new_contact__title">Add new contact</div>
				<div className="add_new_contact__form">
					<form onSubmit={(e) => this.addContact(e)}>
						<div className="form__row">
							<label htmlFor="name">Name:</label>
							<input onChange={(e) => this.inputFormHandler(e)} value={this.state.contact.name} required
							       id="name" name="name" type="text"/>
						</div>
						<div className="form__row">
							<label htmlFor="phone">Phone:</label>
							<input onChange={(e) => this.inputFormHandler(e)} value={this.state.contact.phone} required
							       id="phone" name="phone" type="number"/>
						</div>
						<div className="form__row">
							<label htmlFor="email">E-mail:</label>
							<input onChange={(e) => this.inputFormHandler(e)} value={this.state.contact.email} required
							       id="email" name="email" type="email"/>
						</div>
						<div className="form__row">
							<label htmlFor="photo">Photo:</label>
							<input onChange={(e) => this.inputFormHandler(e)} value={this.state.contact.photo}
							       id="photo" name="photo" type="text"/>
						</div>
						<div className="form__row photo_preview">
							<label>Photo preview:</label>
							<div className="photo_preview__image">
								<img src={this.state.contact.photo} alt="contact avatar"/>
							</div>
						</div>
						<div className="form__row buttons">
							<button>Save</button>
							<NavLink exact to="/">Back to contacts</NavLink>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		contacts: state.list.contacts
	}
};

const mapDispatchToProps = dispatch => {
	return {
		addContactHandler: (contact) => dispatch(addContactHandler(contact)),
		editContactHandler: (id, contact) => dispatch(editContactHandler(id, contact)),
		updateContact: () => dispatch(updateContact())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewContact);