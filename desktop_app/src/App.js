import React, {Component, Fragment} from 'react';
import Header from "./components/Header/Header";
import {Route, Switch} from "react-router-dom";
import ContactList from "./container/ContactList/ContactList";
import AddNewContact from "./container/AddNewContact/AddNewContact";
class App extends Component {
	render() {
		return (
			<Fragment>
				<Header/>
				<Switch>
					<Route exact path="/" component={ContactList}/>
					<Route exact path="/add_new_contact" component={AddNewContact}/>
					<Route exact path="/add_new_contact/:id" component={AddNewContact}/>
				</Switch>
			</Fragment>
		);
	}
}

export default App;
