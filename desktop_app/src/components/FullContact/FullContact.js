import React, {Component, Fragment} from 'react';
import './FullContact.css'
import {connect} from "react-redux";
import {deleteContact, purchaseCancelHandler} from "../../store/actions/actionsList";

class FullContact extends Component {
	state = {
		contact: {
			name: '',
			phone: '',
			email: '',
			photo: ''
		}
	};
	
	componentDidUpdate() {
		let newContact = this.props.contacts[this.props.id];
		if (this.props.contacts[this.props.id] && (this.state.contact !== newContact)) {
			let contact = {...this.state.contact};
			contact = newContact;
			this.setState({contact});
		}
	}
	
	deleteHandler = (id) => {
		this.props.deleteContact(id);
		this.props.purchaseCancelHandler();
	};
	
	editContact = (id) => {
		this.props.history.push({pathname: '/add_new_contact/' + id});
		this.props.purchaseCancelHandler();
	};
	
	render() {
		return (
			<Fragment>
				<div className="full_contact">
					<div className="image">
						<img src={this.state.contact.photo} alt="avatar"/>
					</div>
					<div className="info">
						<span className="name">Name: {this.state.contact.name}</span>
						<span className="phone">Phone: <a
							href={'tel:' + this.state.contact.phone}>{this.state.contact.phone}</a></span>
						<span className="email">E-mail: <a
							href={'mailto:' + this.state.contact.email}>{this.state.contact.email}</a></span>
					</div>
				</div>
				<div className="buttons">
					<button onClick={() => this.editContact(this.props.id)}>Edit</button>
					<button onClick={() => this.deleteHandler(this.props.id)}>Delete</button>
				</div>
			</Fragment>
		)
	}
}

const mapStateToProps = state => {
	return {
		contacts: state.list.contacts
	}
};

const mapDispatchToProps = dispatch => {
	return {
		deleteContact: (id) => dispatch(deleteContact(id)),
		purchaseCancelHandler: () => dispatch(purchaseCancelHandler())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(FullContact);