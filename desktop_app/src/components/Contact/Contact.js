import React from 'react';
import './Contact.css';

const Contact = props => {
	return(
		<li className="contact" onClick={props.click}>
			<div className="image"><img src={props.url} alt="avatar"/></div>
			<div className="name">{props.name}</div>
		</li>
	)
};

export default Contact;