import React,{Component} from 'react'
import {NavLink} from "react-router-dom";
import './Header.css';

class Header extends Component {
	render(){
		return(
			<header>
				<div className="logo">
					<NavLink exact to="/">Contacts</NavLink>
				</div>
				<div className="button">
					<NavLink exact to="/add_new_contact">Add new contact</NavLink>
				</div>
			</header>
		)
	}
}

export default Header;