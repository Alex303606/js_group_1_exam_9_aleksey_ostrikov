import axios from 'axios';
import * as actionsTypes from './actionsTypes';

export const contactsRequest = () => {
	return {type: actionsTypes.ADD_CONTACT_REQUEST};
};

export const contactsSuccess = () => {
	return {type: actionsTypes.ADD_CONTACT_SUCCESS};
};

export const contactsError = (error) => {
	return {type: actionsTypes.ADD_CONTACT_ERROR, error};
};

export const addContactHandler = contact => {
	return (dispatch) => {
		dispatch(contactsRequest());
		axios.post('/contacts.json', contact).then(response => {
			dispatch(contactsSuccess());
		}, error => {
			dispatch(contactsError(error));
		});
	}
};

export const editContactHandler = (id, contact) => {
	return (dispatch) => {
		dispatch(contactsRequest());
		axios.put(`/contacts/${id}.json`, contact).then(response => {
			dispatch(contactsSuccess());
		}, error => {
			dispatch(contactsError(error));
		});
	}
};

export const getContactHandler = (id, contact) => {
	return (dispatch) => {
		dispatch(contactsRequest());
		axios.get(`/contacts/${id}.json`, contact).then(response => {
			dispatch(contactsSuccess());
		}, error => {
			dispatch(contactsError(error));
		});
	}
};