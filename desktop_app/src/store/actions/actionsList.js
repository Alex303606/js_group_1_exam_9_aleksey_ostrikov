import axios from 'axios';
import * as actionsTypes from './actionsTypes';

export const contactsRequest = () => {
	return {type: actionsTypes.ADD_CONTACT_REQUEST};
};

export const contactsUpdate = (contacts) => {
	return {type: actionsTypes.ADD_CONTACT_UPDATE, contacts};
};

export const contactsError = (error) => {
	return {type: actionsTypes.ADD_CONTACT_ERROR, error};
};

export const purchaseHandler = () => {
	return {type: actionsTypes.PURCHASING_TRUE};
};

export const purchaseCancelHandler = () => {
	return {type: actionsTypes.PURCHASING_FALSE};
};

export const updateContact = () => {
	return (dispatch) => {
		dispatch(contactsRequest());
		axios.get('/contacts.json').then(response => {
			if (response.data) dispatch(contactsUpdate(response.data));
		}, error => {
			dispatch(contactsError(error));
		});
	}
};

export const deleteContact = (id) => {
	return (dispatch) => {
		dispatch(contactsRequest());
		axios.delete(`/contacts/${id}.json`).then(() => dispatch(updateContact()));
	}
};