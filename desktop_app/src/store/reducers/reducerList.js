import * as actionTypes from '../actions/actionsTypes';

const initialState = {
	contacts: {},
	loading: false,
	error: null,
	purchasing: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.ADD_CONTACT_REQUEST:
			return {...state, loading: true, error: null};
		case actionTypes.ADD_CONTACT_UPDATE:
			return {...state, loading: false, error: null, contacts: action.contacts};
		case actionTypes.ADD_CONTACT_ERROR:
			return {...state, loading: false, error: action.error};
		case actionTypes.PURCHASING_TRUE:
			return {...state, purchasing: true};
		case actionTypes.PURCHASING_FALSE:
			return {...state, purchasing: false};
		default:
			return state;
	}
};

export default reducer;