import * as actionTypes from '../actions/actionsTypes';

const initialState = {
	loading: false,
	error: null,
	purchasing: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.ADD_CONTACT_REQUEST:
			return {...state, loading: true, error: null};
		case actionTypes.ADD_CONTACT_SUCCESS:
			return {...state, loading: false, error: null, purchasing: false};
		case actionTypes.ADD_CONTACT_ERROR:
			return {...state, loading: false, error: action.error};
		default:
			return state;
	}
};

export default reducer;