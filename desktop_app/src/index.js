import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from "react-router-dom";
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import reducerAdd from './store/reducers/reducerAdd';
import reducerList from './store/reducers/reducerList';
import thunk from 'redux-thunk';
import axios from "axios";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
	add: reducerAdd,
	list: reducerList
});

axios.defaults.baseURL = 'https://contacts-83436.firebaseio.com/';

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

const Application = (
	<BrowserRouter>
		<Provider store={store}>
			<App/>
		</Provider>
	</BrowserRouter>
);

ReactDOM.render(Application, document.getElementById('root'));
registerServiceWorker();
